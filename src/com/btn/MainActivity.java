package com.btn;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import java.util.ArrayList;

public class MainActivity extends Activity
{
    private String disp;
    private String nums;
    private char op;
    private ArrayList<Integer> num;
    
    public MainActivity() {
        num = new ArrayList<Integer>();
        disp = "";
        op='+';
        nums="";
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
    }
    
    public void HandleClick(View view) {
	setDisplay(view);
    }
    
    
    private void setDisplay(View view){
        Button b = (Button)view;
        EditText et = (EditText) findViewById(R.id.display);
        
        switch(b.getId())
        {
            case R.id.add: 
                setOprtn('+', b.getText().toString());
                break;
            case R.id.sub:
                setOprtn('-', b.getText().toString());
                break;
            case R.id.mul:
               setOprtn('*', b.getText().toString());
                break;
            case R.id.div:
                setOprtn('/', b.getText().toString());
                break;
            case R.id.equal:
                if(nums.length()>0){
                    num.add(Integer.parseInt(nums));
                    nums="";
                    setInput();
                }
                break;
            case R.id.cancel:
                num.clear();
                disp = "";
                nums="";
                break;
            case R.id.zero:
                nums+="0";
                disp += "0"; 
                break;
            case R.id.one:
                nums+="1";
                disp += "1";
                break;
            case R.id.two:
                nums+="2";
                disp += "2";
                break;
            case R.id.three:
                nums+="3";
                disp += "3";
                break;
            case R.id.four:
                nums+="4";
                disp += "4";
                break;
            case R.id.five:
                nums+="5";
                disp += "5";
                break;
            case R.id.six:
                nums+="6";
                disp += "6";
                break;
            case R.id.seven:
                nums+="7";
                disp += "7";
                break;
            case R.id.eight:
                nums+="8";
                disp += "8";
                break;
            case R.id.nine:
                nums+="9";
                disp += "9";
                break;            
        }
        et.setText(disp);
    }
    
    private void setOprtn(char p,String t){
        if(checkInput()){
            setInput();
            disp +=t;
            op=p;
        }
    }
    
    private Boolean checkInput(){
        return (disp.endsWith("+") || disp.endsWith("-") || disp.endsWith("*") || disp.endsWith("/")  )? false : true;
    }
    
    private void setInput(){
        
        if(!nums.isEmpty()){
            num.add(Integer.parseInt(nums));
            nums="";
        }
        if(num.size()==2){
            Result();
            disp = num.get(0).toString();
        }
    }
        
    private void Result(){
         int res = 0;
         switch(op)
         {
            case '+':
                res = num.get(0) + num.get(1);
                break;
            case '-':
                res = num.get(0) - num.get(1);
                break;
            case '*':
                res = num.get(0) * num.get(1);
                break;
            case '/':
                res = num.get(0) / num.get(1);
                break;
         }
         num.clear();
         num.add(res);         
     }
    
    
}
